package com.application.rest.resource;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.application.rest.dto.LancamentoEstatisticaCategoria;
import com.application.rest.dto.LancamentoEstatisticaDia;
import com.application.rest.dto.LancamentoEstatisticaPessoa;
import com.application.rest.model.Lancamento;
import com.application.rest.projection.LancamentoResumo;
import com.application.rest.repository.LancamentoFilter;
import com.application.rest.repository.LancamentoRepository;
import com.application.rest.service.LancamentoService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/lancamentos")
public class LancamentoResource {

	@Autowired
	private LancamentoRepository lancamentoRepository;
	
	@Autowired
	private LancamentoService lancamentoService;
	
	@GetMapping
	public Page<Lancamento> pesquisar(@RequestParam(value = "page" , defaultValue="0") int page,
	                                  @RequestParam(value = "size" , defaultValue="5") int size,
	                                  LancamentoFilter lancamentoFilter) {
	    Pageable pageable = PageRequest.of(page, size);                           	
		return lancamentoRepository.filtrar(lancamentoFilter, pageable);
	}
	
	@GetMapping(params = "resumo")
	public Page<LancamentoResumo> resumir(@RequestParam(value="page" , defaultValue="0") int page,
										  @RequestParam(value="size" , defaultValue="5") int size, 
										  LancamentoFilter lancamentoFilter) {
		Pageable pageable = PageRequest.of(page, size);
		return lancamentoRepository.resumir(lancamentoFilter, pageable);
	}
	
	@GetMapping("/estatistica/por-categoria")
	public List<LancamentoEstatisticaCategoria> porCategoria(){
		return this.lancamentoRepository.porCategoria(LocalDate.now());
	}
	
	@GetMapping("/estatistica/por-dia")
	public List<LancamentoEstatisticaDia> porDia(){
		return this.lancamentoRepository.porDia(LocalDate.now());
	}
	
	@GetMapping("/estatistica/por-pessoa")
	public List<LancamentoEstatisticaPessoa> porPessoa(@RequestParam(value = "inicio") 
													   @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate inicio,
			                                           @RequestParam(value = "fim") 
	                                                   @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fim){
		return this.lancamentoRepository.porPessoa(inicio, fim);
	}
	
	
	@GetMapping("/{codigo}")
	public Lancamento buscarPorCodigo(@PathVariable Long codigo) {
		Lancamento lancamento = lancamentoRepository.findByCodigo(codigo);
		return lancamento;
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Lancamento salvar(@Valid @RequestBody Lancamento lancamento) {
		return lancamentoService.salvar(lancamento);
	}
	
	@PutMapping("/{codigo}")
	public Lancamento editar(@Valid @PathVariable Long codigo , @RequestBody Lancamento lancamento) {
		return lancamentoService.atualizar(codigo, lancamento);
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable Long codigo) {
		Lancamento lancamento = lancamentoRepository.findByCodigo(codigo);
		lancamentoRepository.delete(lancamento);
	}
}