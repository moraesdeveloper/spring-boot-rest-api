package com.application.rest.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.application.rest.model.Categoria;
import com.application.rest.repository.CategoriaRepository;
import com.application.rest.service.CategoriaService;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("/categorias")
public class CategoriaResource {

	@Autowired
	private CategoriaRepository categoriaRepository;
	@Autowired
	private CategoriaService categoriaService;
	
	@GetMapping
	public Page<Categoria> listar(@RequestParam(value="page", defaultValue="0") int page,
								  @RequestParam(value="size", defaultValue="5") int size) {
		Pageable pageable = PageRequest.of(page, size);
		return categoriaRepository.findAll(pageable);	
	}
	
	@GetMapping("/{codigo}")
	public Categoria buscarPorCodigo(@PathVariable Long codigo) {
		Categoria categoria = categoriaRepository.findByCodigo(codigo);
		return categoria;
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Categoria salvar(@Valid @RequestBody Categoria categoria) {
		return categoriaRepository.save(categoria);
	}
	
	@PutMapping("/{codigo}")
	public Categoria editar(@Valid @PathVariable Long codigo,  @RequestBody Categoria categoria) {
		return categoriaService.atualizar(codigo, categoria);
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable Long codigo) {
		Categoria CategoriaDeletar = categoriaRepository.findByCodigo(codigo);
		categoriaRepository.delete(CategoriaDeletar);
	}
	
}