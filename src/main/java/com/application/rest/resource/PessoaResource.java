package com.application.rest.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.application.rest.model.Pessoa;
import com.application.rest.repository.PessoaRepository;
import com.application.rest.service.PessoaService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/pessoas")
public class PessoaResource {

	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Autowired
	private PessoaService pessoaService;
	
	@GetMapping
	public Page<Pessoa> listar(@RequestParam(value="page", defaultValue="0") int page,
							   @RequestParam(value="size", defaultValue="5") int size) {
		Pageable pageable = PageRequest.of(page, size);
		return pessoaRepository.findAll(pageable);
	}
	
	@GetMapping("/{codigo}")
	public Pessoa buscarPorCodigo(@PathVariable Long codigo) {
		Pessoa pessoa = pessoaRepository.findByCodigo(codigo);
		return pessoa;
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Pessoa salvar(@Valid @RequestBody Pessoa pessoa) {
		return pessoaRepository.save(pessoa);
	}
	
	@PutMapping("/{codigo}")
	public Pessoa editar(@Valid @PathVariable Long codigo , @RequestBody Pessoa pessoa) {
		return pessoaService.atualizar(codigo, pessoa);
	}
	
	@PutMapping("/{codigo}/ativo")
	public void atualizarStatus(@Valid @PathVariable Long codigo, @RequestBody Boolean ativo) {
		pessoaService.atualizarStatus(codigo, ativo);
		
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable Long codigo) {
		Pessoa pessoaDeletada = pessoaRepository.findByCodigo(codigo);
		pessoaRepository.delete(pessoaDeletada);
	}
	
}