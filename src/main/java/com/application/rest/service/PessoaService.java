package com.application.rest.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.application.rest.model.Pessoa;
import com.application.rest.repository.PessoaRepository;

@Service
public class PessoaService {

	@Autowired
	private PessoaRepository pessoaRepository;
	
	
	public void atualizarStatus(Long codigo, Boolean ativo) {
		Pessoa pessoa = pessoaRepository.findByCodigo(codigo);
		pessoa.setAtivo(ativo);
		pessoaRepository.save(pessoa);
	}
	
	public Pessoa atualizar(Long codigo, Pessoa pessoa) {
		Pessoa pessoaAtualizada = pessoaRepository.findByCodigo(codigo);
		BeanUtils.copyProperties(pessoa, pessoaAtualizada, "codigo");
		pessoaRepository.save(pessoaAtualizada);
		return pessoaAtualizada;
	}
	
}
