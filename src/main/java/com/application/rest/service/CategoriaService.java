package com.application.rest.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.application.rest.model.Categoria;
import com.application.rest.repository.CategoriaRepository;

@Service
public class CategoriaService {

	@Autowired
	private CategoriaRepository categoriaRepository;
	
	
	public Categoria atualizar(Long codigo , Categoria categoria) {
		Categoria categoriaAtualizada = categoriaRepository.findByCodigo(codigo);
		BeanUtils.copyProperties(categoria, categoriaAtualizada, "codigo");
		categoriaRepository.save(categoriaAtualizada);
		return categoriaAtualizada;
	}
	
	
}
