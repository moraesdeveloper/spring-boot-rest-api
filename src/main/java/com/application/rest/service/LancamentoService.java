package com.application.rest.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.application.rest.model.Lancamento;
import com.application.rest.model.Pessoa;
import com.application.rest.repository.LancamentoRepository;
import com.application.rest.repository.PessoaRepository;
import com.application.rest.service.exception.PessoaInexistenteOuInativaException;

@Service
public class LancamentoService {

	@Autowired
	private LancamentoRepository lancamentoRepository;
	@Autowired
	private PessoaRepository pessoaRepository;
	
	
	public Lancamento salvar(Lancamento lancamento) {
		validarPessoa(lancamento);
		return lancamentoRepository.save(lancamento);
	}
	
	
	private void validarPessoa(Lancamento lancamento) {
		Pessoa pessoa = null;
		if(lancamento.getPessoa().getCodigo() != null) {
			pessoa = pessoaRepository.findByCodigo(lancamento.getPessoa().getCodigo());
		}
		if(pessoa == null || !pessoa.isAtivo()) {
			throw new PessoaInexistenteOuInativaException("Pessoa Inexistente ou Inativa");
		}
	}
	
	public Lancamento atualizar(Long codigo, Lancamento lancamento) {
		Lancamento lancamentoAtualizado = lancamentoRepository.findByCodigo(codigo);
		BeanUtils.copyProperties(lancamento, lancamentoAtualizado, "codigo");
		lancamentoRepository.save(lancamentoAtualizado);
		return lancamentoAtualizado;
	}
	
}
