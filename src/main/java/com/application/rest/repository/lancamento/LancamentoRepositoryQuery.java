package com.application.rest.repository.lancamento;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.application.rest.dto.LancamentoEstatisticaCategoria;
import com.application.rest.dto.LancamentoEstatisticaDia;
import com.application.rest.dto.LancamentoEstatisticaPessoa;
import com.application.rest.model.Lancamento;
import com.application.rest.projection.LancamentoResumo;
import com.application.rest.repository.LancamentoFilter;

public interface LancamentoRepositoryQuery {
	
	public List<LancamentoEstatisticaCategoria> porCategoria(LocalDate mesReferencia);
	
	public List<LancamentoEstatisticaDia> porDia(LocalDate mesReferencia);
	
	public List<LancamentoEstatisticaPessoa> porPessoa(LocalDate inicio, LocalDate fim);
	
	public Page<LancamentoResumo> resumir(LancamentoFilter lancamentoFilter, Pageable pageable);
	
	public Page<Lancamento> filtrar(LancamentoFilter lancamentoFilter, Pageable pageable);

}