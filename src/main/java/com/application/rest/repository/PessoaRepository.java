package com.application.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.application.rest.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

	Pessoa findByCodigo(Long codigo);
	
}
