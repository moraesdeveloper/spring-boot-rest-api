package com.application.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.application.rest.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

	Categoria findByCodigo(Long codigo);
}
