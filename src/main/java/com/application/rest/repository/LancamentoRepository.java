package com.application.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.application.rest.model.Lancamento;
import com.application.rest.repository.lancamento.LancamentoRepositoryQuery;

public interface LancamentoRepository extends JpaRepository<Lancamento, Long>, LancamentoRepositoryQuery {

	Lancamento findByCodigo(Long codigo);
	
}
