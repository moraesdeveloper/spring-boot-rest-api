package com.application.rest.model;

public enum TipoLancamento {
	
	RECEITA,
	DESPESA;

}
